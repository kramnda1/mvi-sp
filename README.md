### Zadání
Zadaním je zmapovat existující řešení segmentace ohně ze snímků a natrénovat některou z architektur na libovolném datasetu. Tato znalost bude následně použita v budoucím projektu pro lokalizaci ohně, ohniska a vznikajících ohňů ze snímků/videa a tvorbě vlastního datastu. Pozice segmentovanéh ohně může být následně použita pro automatizovaný hasící systém.

### Pokyny ke spuštění
Soubor `fire_segmentation_training.ipynb` je kopií notebooku z platformy google colab. Obsahuje všechny potřebná data a informace k spuštění. Pro spuštění je potřeba zkopírovat notebook do google colab a spustit. Natrénovaný model s nejlepšími výsledky je uložený v souboru `best_model.pth`. V souboru `config.py` jsou uloženy všechny parametry, které byly použity při trénování modelu.

Dané soubory je možno stáhnout na tomto odkazu: https://drive.google.com/file/d/1b3hkxkXzRpMHzWEaAzRU9C-8tKPoS_cQ/view?usp=sharing.