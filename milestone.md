## Milestone
### Zadání
Zadaním je zmapovat existující řešení segmentace ohně ze snímků a natrénovat některou z architektur na libovolném datasetu. Tato znalost bude následně použita v budoucím projektu pro lokalizaci ohně, ohniska a vznikajících ohňů ze snímků/videa a tvorbě vlastního datastu. Pozice segmentovanéh ohně může být následně použita pro automatizovaný hasící systém.

### Exisujicí datasety

Existujě několik veřejně přístupných datasetů. Souhrn existujících datasetů můžeme najít zde [^fn1]. Výstupem tohoto paperu je state of the art dataset [^fn2] obsahující 1775 snímků obsahujících požár, pro které jsou vytvořeny polygonové anotace. Datová sada je rozšířena o snímky bez požáru z datasetu SUN397. 

Dále ještě existuje několi dalších datasetů, které mouhou být nalezeny na těchto odkazech: [^fn3][^fn4][^fn5][^fn6][^fn7].

### Přečtené články 

Existuje několik článků, jenž se zabývají segmentací ohně. Některé ze starších přístupů používají předeším tradičních metod zpracování obrazu. Dnes jsou již tradiční metody na ústupu, přesto je však možné je použít pro tvorbu anatocí na snímcích [^fn8]. Souhrn především tradičních metod se nachází zde [^fn9].


V [^fn10] je souhrn state of the art metod sémantické segmentace. Metody sémantické segmentace jsou použity v [^fn1][^fn11][^fn12][^fn13]. Metody instační segmentace jsou použity v [^fn14].

### Impementace 

Byl proveden průzkum dostupných implementací a pro instanční segmentaci se mi jeví jako dobrá volba Detectron2 založený na Pytorch s implementací MASK RCNN. Detectron2 jsem použil k jednoduché segmentaci nafukovacíh balónků v prostředí Google Colab [^fn15]. Následně jsem narazil na MMSegmentation sada nástrojů pro sémantickou segmentaci založená na PyTorchi. Dále bych se chtěl zaměřit na sémantickou segmentaci a natrénovat model za použití jedné z těchto architektur UNet, YOLOv7, DeepLabV3+, případně pokračovat s instační segmentací a MASK RCNN.

[^fn1]: https://link.springer.com/chapter/10.1007/978-3-030-40605-9_3
[^fn2]: http://merlin.fit.vutbr.cz/fire/ 
[^fn3]: https://www.kaggle.com/datasets/diversisai/fire-segmentation-image-dataset
[^fn4]: https://paperswithcode.com/dataset/flame
[^fn5]: https://ieee-dataport.org/open-access/large-scale-dataset-active-fire-detectionsegmentation-landsat-8
[^fn6]: https://github.com/AlirezaShamsoshoara/Fire-Detection-UAV-Aerial-Image-Classification-Segmentation-UnmannedAerialVehicle
[^fn7]: https://mivia.unisa.it/datasets/video-analysis-datasets/fire-detection-dataset/
[^fn8]: https://ieeexplore.ieee.org/document/9494421
[^fn9]: https://www.sciencedirect.com/science/article/pii/S1051200413001462?via%3Dihub
[^fn10]: https://www.researchgate.net/publication/357761289_Review_the_state-of-the-art_technologies_of_semantic_segmentation_based_on_deep_learning
[^fn11]: https://eurasip.org/Proceedings/Eusipco/Eusipco2021/pdfs/0000741.pdf
[^fn12]: https://file.techscience.com/ueditor/files/cmc/TSP_CMC-72-3/TSP_CMC_26498/TSP_CMC_26498.pdf
[^fn13]: https://ietresearch.onlinelibrary.wiley.com/doi/full/10.1049/ipr2.12046
[^fn14]: https://ieeexplore.ieee.org/document/9598928
[^fn15]: https://ieeexplore.ieee.org/document/9598928